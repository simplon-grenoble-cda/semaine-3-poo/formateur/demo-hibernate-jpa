
Cette demo suppose qu'il y a un serveur MySQL qui tourne en local, accessible à l'adresse 127.0.0.1:3306,
avec une base de données existante `book_test`, et l'utilisateur `root` dont le mot de passe est `root`.

Lancer le programme via `mvn javafx:run`, le programme devrait créer la table `Book` et insérer dedans deux lignes.

On peut éditer le fichier `persistence.xml` pour modifier les paramètres d'accès à la base de données (proposer
un nom de base de données différent, des identifiants de connexion à la base de données différents…)
