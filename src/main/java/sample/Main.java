package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.Date;
import java.util.List;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root, 300, 275));
        primaryStage.show();
    }

    public static void main(String[] args) {
        //launch(args);


        // Demo Hibernate en mode JPA

        // like discussed with regards to SessionFactory, an EntityManagerFactory is set up once for an application
        // 		IMPORTANT: notice how the name here matches the name we gave the persistence-unit in persistence.xml!
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory( "sample" );


        // Créer deux instances Book et les persister dans la base de données
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.persist(new Book( "Premier livre"));
        entityManager.persist(new Book( "Second livre"));
        entityManager.getTransaction().commit();
        entityManager.close();

        // Faire en requête dans la base de données pour obtenir tous les livres qui y sont enregistrés
        entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        List<Book> result = entityManager.createQuery( "from Book", Book.class ).getResultList();
        for (Book event : result) {
            System.out.println("Book (" + event.getTitle() + ")");
        }
        entityManager.getTransaction().commit();
        entityManager.close();

        entityManagerFactory.close();
    }
}
